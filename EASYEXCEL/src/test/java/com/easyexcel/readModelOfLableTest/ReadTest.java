package com.easyexcel.readModelOfLableTest;

import com.easyexcel.read.ReadExcel;
import com.easyexcel.read.ReadListExcel;
import com.easyexcel.read.ReadModelExcel;
import com.easyexcel.read.exception.LackCellAnnotation;
import javafx.beans.binding.ObjectExpression;
import org.junit.Before;
import org.junit.Test;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ReadTest {
	@Before
	public void setUp() throws Exception {
	}

	@Test
	public void test() throws InstantiationException, IllegalAccessException, IOException, LackCellAnnotation{
		ReadExcel<Students> re = new ReadModelExcel<>();
		Map<String,Object> param = new HashMap<String,Object>();
		param.put("inFilePath","d:\\studentsModel.xlsx");
		List<Students> list = re.read(param,Students.class);
		System.out.println(list);
	}
}
