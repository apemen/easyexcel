package com.easyexcel.writemodeloflaabletest;

import java.util.ArrayList;
import java.util.List;

import com.easyexcel.annotation.Cell;
import com.easyexcel.annotation.ListExcel;
import com.easyexcel.annotation.Select;

public class Students {
	private String name;
	@Cell(name = "age")
    private int nl;
	@ListExcel(beginRowName="grade")
	private List<Grade> grades = new ArrayList<Grade>();
	@Select
	private String[] topics = {"语文","数学"};
    public String[] getTopics() {
		return topics;
	}

	public void setTopics(String[] topics) {
		this.topics = topics;
	}

	public List<Grade> getGrades() {
		return grades;
	}

	public void setGrades(List<Grade> grades) {
		this.grades = grades;
	}

	public int getNl() {
        return nl;
    }

    public void setNl(int nl) {
        this.nl = nl;
    }

    public Students(String name, int age){
        this.name = name;
        this.nl = age;
    }
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}

}
