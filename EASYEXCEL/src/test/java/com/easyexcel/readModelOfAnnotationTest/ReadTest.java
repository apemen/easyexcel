package com.easyexcel.readModelOfAnnotationTest;

import com.easyexcel.read.ReadExcel;
import com.easyexcel.read.ReadListExcel;
import com.easyexcel.read.ReadModelExcel;
import com.easyexcel.read.exception.LackCellAnnotation;
import org.junit.Before;
import org.junit.Test;

import java.io.IOException;
import java.util.List;

public class ReadTest {
	@Before
	public void setUp() throws Exception {
	}

	@Test
	public void test() throws InstantiationException, IllegalAccessException, IOException, LackCellAnnotation{
		ReadExcel<Students> re = new ReadModelExcel<>();
		List<Students> list = re.read(null,Students.class);
		System.out.println(list);
	}
}
