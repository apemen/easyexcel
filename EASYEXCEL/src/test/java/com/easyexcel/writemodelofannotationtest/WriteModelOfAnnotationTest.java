package com.easyexcel.writemodelofannotationtest;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;

import com.easyexcel.write.WriteExcel;
import com.easyexcel.write.WriteModelExcel;

public class WriteModelOfAnnotationTest {

	@Before
	public void setUp() throws Exception {
	}

	@Test
	public void writeModelOfAnnotationTest() throws IllegalArgumentException, IllegalAccessException, IOException {
		WriteExcel we = new WriteModelExcel();
		Map<String,Object> param = new HashMap<>();
//		param.put("inFilePath", "e:\\model.xlsx");//读取文件的目录必须有，可以传也可以用注解配,如果传以传为主
//		param.put("outFilePath", "e:\\outmodel.xlsx");//生成路径必须有，可以传也可以用注解配,如果传以传为主
		List<Students> list = new ArrayList<Students>();
		Students students = new Students("张三2222222222222222222222222222222",25);
		List<Grade> grades = new ArrayList<Grade>();
		Grade grade = new Grade("数学",99);
		grades.add(grade);
		students.setGrades(grades);
		list.add(students);
		we.write(param, list);
	}

}
