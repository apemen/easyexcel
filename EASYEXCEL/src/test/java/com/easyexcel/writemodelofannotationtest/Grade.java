package com.easyexcel.writemodelofannotationtest;

import com.easyexcel.annotation.Cell;
import com.easyexcel.annotation.Excel;

@Excel(beginRow=4/*,dataHeader="课目:1,分数:2"*/,createRowWay="insert")
public class Grade {
	@Cell(columnNum="1")
	private String topic;
	@Cell(columnNum="2")
	private double score;
	
	public Grade(String topic,double score){
		this.topic = topic;
		this.score = score;
	}
	public String getTopic() {
		return topic;
	}
	public void setTopic(String topic) {
		this.topic = topic;
	}
	public double getScore() {
		return score;
	}
	public void setScore(double score) {
		this.score = score;
	}
	
}
