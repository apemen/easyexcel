package com.easyexcel.writemodelofannotationtest;

import java.util.ArrayList;
import java.util.List;

import com.easyexcel.annotation.Cell;
import com.easyexcel.annotation.Excel;
import com.easyexcel.annotation.Ingroe;
import com.easyexcel.annotation.ListExcel;
import com.easyexcel.annotation.Select;
import com.easyexcel.annotation.Sheet;
@Excel(inFilePath="d:\\modelann.xlsx",outFilePath="d:\\outmodelann.xlsx")
@Sheet(sheetNum=1)
public class Students {
	@Cell(rowNum=1,columnNum="b")
	private String name;
	@Cell(rowNum=3,columnNum="b")
	private int age;
	@Ingroe
	private double  price;
	@ListExcel
	private List<Grade> grades = new ArrayList<Grade>();
	@Select
	@Cell(rowNum=5,columnNum="b")
	private String[] topics = {"语文","数学"};
	public double getPrice() {
		return price;
	}
	public void setPrice(double price) {
		this.price = price;
	}
	public String[] getTopics() {
		return topics;
	}
	public void setTopics(String[] topics) {
		this.topics = topics;
	}
	public Students(String name,int age){
		this.name = name;
		this.age = age;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public int getAge() {
		return age;
	}
	public void setAge(int age) {
		this.age = age;
	}
	public List<Grade> getGrades() {
		return grades;
	}
	public void setGrades(List<Grade> grades) {
		this.grades = grades;
	}
}
